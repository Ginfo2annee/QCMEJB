/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Reponses;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author kabbaj
 */
@Stateless
public class ReponsesFacade extends AbstractFacade<Reponses> {

    @PersistenceContext(unitName = "Examen-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReponsesFacade() {
        super(Reponses.class);
    }
    public List<Reponses> BonneReponse(){
        Query query = em.createNamedQuery("BonneReponse.findIdQuestion");
        List<Reponses> r=query.getResultList();
        return r;
    }
    
    
}
