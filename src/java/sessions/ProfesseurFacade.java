/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Etudiants;
import entities.Professeur;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
/**
 *
 * @author Ichraq
 */
@Stateless
public class ProfesseurFacade extends AbstractFacade<Professeur> {

    @PersistenceContext(unitName = "Examen-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProfesseurFacade() {
        super(Professeur.class);
    }
    
    public Professeur connect(String email,String password){
         Query query = em.createNamedQuery("Professeur.Login").setParameter("email",email).setParameter("password",password);
         Professeur prof = (Professeur) query.getSingleResult();
         return prof;
                 
    }
        
}
