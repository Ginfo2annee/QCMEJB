/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Examens;
import entities.Questions;
import entities.Reponses;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author kabbaj
 */
@Stateless
public class QuestionsFacade extends AbstractFacade<Questions> {

    @PersistenceContext(unitName = "Examen-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public QuestionsFacade() {
        super(Questions.class);
    }
    public List<Questions> Question(Examens idExamen)
    {
        
            Query query= em.createNamedQuery("Questions.findByExamen");
            query.setParameter("idExamen",idExamen);            
                    return query.getResultList();
        
    }
    public List<Questions> QuestionBonneReponse(int idExamen){
        Query query=em.createNamedQuery("Question.findByBonneReponse").setParameter("idExamen",idExamen);
        List<Questions>q=query.getResultList();
        
        return q;
    }
    public List<Reponses> Reponses(){
        Query query=em.createNamedQuery("Question.findAllReponse");
        List<Reponses> r = query.getResultList();
        return r;
    }
   
    
}
