/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Etudiants;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author kabbaj
 */
@Stateless
public class EtudiantsFacade extends AbstractFacade<Etudiants> {

    @PersistenceContext(unitName = "Examen-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EtudiantsFacade() {
        super(Etudiants.class);
    }
    
    public Etudiants connect(String email,String password){
         Query query = em.createNamedQuery("Etudiants.Login").setParameter("email",email).setParameter("password",password);
         Etudiants etudiant = (Etudiants) query.getSingleResult();
         return etudiant;
                 
    }
        
}
