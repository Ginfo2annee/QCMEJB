/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Examens;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kabbaj
 */
@Stateless
public class ExamensFacade extends AbstractFacade<Examens> {

    @PersistenceContext(unitName = "Examen-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ExamensFacade() {
        super(Examens.class);
    }
    
    
}
