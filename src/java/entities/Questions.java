/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kabbaj
 */
@Entity
@Table(name = "questions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Questions.findAll", query = "SELECT q FROM Questions q")
    , @NamedQuery(name = "Questions.findByIdQuestions", query = "SELECT q FROM Questions q WHERE q.idQuestions = :idQuestions")
    , @NamedQuery(name = "Questions.findByQuestion", query = "SELECT q FROM Questions q WHERE q.question = :question")
    , @NamedQuery(name = "Questions.findByConsigne", query = "SELECT q FROM Questions q WHERE q.consigne = :consigne")
    , @NamedQuery(name="Questions.findByExamen",query="SELECT q FROM Questions q WHERE q.idExamen=:idExamen")
    , @NamedQuery(name="Question.findByBonneReponse" , query="SELECT q FROM Questions q , Reponses r ,Examens e WHERE q.idQuestions =r.idQuestion AND r.isBonneReponse=1 AND e.idExamen =:idExamen" )
    , @NamedQuery(name="Question.findAllReponse" , query="SELECT q FROM Questions q , Reponses r  WHERE q.idQuestions =r.idQuestion" )
})
public class Questions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idQuestions")
    private Integer idQuestions;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "Question")
    private String question;
    @Size(max = 255)
    @Column(name = "Consigne")
    private String consigne;
    @JoinColumn(name = "idExamen", referencedColumnName = "IdExamen")
    @ManyToOne(optional = false)
    private Examens idExamen;
    

    public Questions() {
    }

    public Questions(Integer idQuestions) {
        this.idQuestions = idQuestions;
    }

    public Questions(Integer idQuestions, String question) {
        this.idQuestions = idQuestions;
        this.question = question;
    }

    public Integer getIdQuestions() {
        return idQuestions;
    }

    public void setIdQuestions(Integer idQuestions) {
        this.idQuestions = idQuestions;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getConsigne() {
        return consigne;
    }

    public void setConsigne(String consigne) {
        this.consigne = consigne;
    }

    public Examens getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(Examens idExamen) {
        this.idExamen = idExamen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idQuestions != null ? idQuestions.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questions)) {
            return false;
        }
        Questions other = (Questions) object;
        if ((this.idQuestions == null && other.idQuestions != null) || (this.idQuestions != null && !this.idQuestions.equals(other.idQuestions))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Questions[ idQuestions=" + idQuestions + " ]";
    }
  
    

    

  
    
}
