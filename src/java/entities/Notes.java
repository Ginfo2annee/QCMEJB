/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kabbaj
 */
@Entity
@Table(name = "notes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Notes.findAll", query = "SELECT n FROM Notes n")
    , @NamedQuery(name = "Notes.findByIdNote", query = "SELECT n FROM Notes n WHERE n.idNote = :idNote")
    , @NamedQuery(name = "Notes.findByNote", query = "SELECT n FROM Notes n WHERE n.note = :note")})
public class Notes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNote")
    private Integer idNote;
    @Column(name = "Note")
    private Integer note;
    @JoinColumn(name = "Matricule", referencedColumnName = "Matricule")
    @ManyToOne(optional = false)
    private Etudiants matricule;
    @JoinColumn(name = "idExamen", referencedColumnName = "IdExamen")
    @ManyToOne(optional = false)
    private Examens idExamen;

    public Notes() {
    }

    public Notes(Integer idNote) {
        this.idNote = idNote;
    }

    public Integer getIdNote() {
        return idNote;
    }

    public void setIdNote(Integer idNote) {
        this.idNote = idNote;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public Etudiants getMatricule() {
        return matricule;
    }

    public void setMatricule(Etudiants matricule) {
        this.matricule = matricule;
    }

    public Examens getIdExamen() {
        return idExamen;
    }

    public void setIdExamen(Examens idExamen) {
        this.idExamen = idExamen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNote != null ? idNote.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notes)) {
            return false;
        }
        Notes other = (Notes) object;
        if ((this.idNote == null && other.idNote != null) || (this.idNote != null && !this.idNote.equals(other.idNote))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Notes[ idNote=" + idNote + " ]";
    }
    
}
