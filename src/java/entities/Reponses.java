/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kabbaj
 */
@Entity
@Table(name = "reponses")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reponses.findAll", query = "SELECT r FROM Reponses r")
    , @NamedQuery(name = "Reponses.findByIdReponse", query = "SELECT r FROM Reponses r WHERE r.idReponse = :idReponse")
    , @NamedQuery(name = "Reponses.findByIdQuestion", query = "SELECT r FROM Reponses r WHERE r.idQuestion = :idQuestion")
    , @NamedQuery(name = "Reponses.findByReponse", query = "SELECT r FROM Reponses r WHERE r.reponse = :reponse")
    , @NamedQuery(name = "Reponses.findByIsBonneReponse", query = "SELECT r FROM Reponses r WHERE r.isBonneReponse = :isBonneReponse")
   , @NamedQuery(name= "BonneReponse.findIdQuestion" , query="SELECT r FROM Reponses r,Questions q WHERE r.isBonneReponse = 1 AND r.idQuestion =q.idQuestions")
})
public class Reponses implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReponse")
    private Integer idReponse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idQuestion")
    private int idQuestion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "Reponse")
    private String reponse;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IsBonneReponse")
    private short isBonneReponse;

    public Reponses() {
    }

    public Reponses(Integer idReponse) {
        this.idReponse = idReponse;
    }

    public Reponses(Integer idReponse, int idQuestion, String reponse, short isBonneReponse) {
        this.idReponse = idReponse;
        this.idQuestion = idQuestion;
        this.reponse = reponse;
        this.isBonneReponse = isBonneReponse;
    }

    public Integer getIdReponse() {
        return idReponse;
    }

    public void setIdReponse(Integer idReponse) {
        this.idReponse = idReponse;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public short getIsBonneReponse() {
        return isBonneReponse;
    }

    public void setIsBonneReponse(short isBonneReponse) {
        this.isBonneReponse = isBonneReponse;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReponse != null ? idReponse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reponses)) {
            return false;
        }
        Reponses other = (Reponses) object;
        if ((this.idReponse == null && other.idReponse != null) || (this.idReponse != null && !this.idReponse.equals(other.idReponse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Reponses[ idReponse=" + idReponse + " ]";
    }
    
}
